       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-GRADE.
       AUTHOR. THANYA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT SCORE-FILE ASSIGN TO "score.dat"
             ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD SCORE-FILE.
       01 SCORE-DETAIL.
           05 STU-ID PIC X(8).
           05 MIDTERM-SCORE PIC 9(2)V9(2).
           05 FINAL-SCORE PIC 9(2)V9(2).
           05 PROJECT-SOCRE PIC 9(2)V9(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "62160272" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SOCRE 
           WRITE SCORE-DETAIL 

           MOVE "62160988" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SOCRE 
           WRITE SCORE-DETAIL 


           MOVE "62160315" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SOCRE 
           WRITE SCORE-DETAIL 

           MOVE "62160101" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SOCRE 
           WRITE SCORE-DETAIL
           CLOSE SCORE-FILE  

           GOBACK 
           .
